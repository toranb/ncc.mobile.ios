#import <UIKit/UIKit.h>

@class NebraskaCodeCampViewController;
@class SessionDetails;
@class SpeakerDetails;
@class Session;
@class Speaker;

@interface NebraskaCodeCampAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow* window;
    NebraskaCodeCampViewController *viewController;
    IBOutlet UINavigationController* navController;
    IBOutlet SessionDetails* detailsController;
    IBOutlet SpeakerDetails* speakerDetailsController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet NebraskaCodeCampViewController *viewController;
@property (nonatomic, retain) SessionDetails* detailsController;
@property (nonatomic, retain) SpeakerDetails* speakerDetailsController;

- (void)sessionJsonFinished;
- (void)showSessionDetailsView:(Session *)session;
- (void)launchSpeakerDetailsView:(Speaker *)speaker;

@end
