#import "SessionDetails.h"
#import "AsyncImageDownload.h"
#import "NebraskaCodeCampAppDelegate.h"
#import "Session.h"
#import "Speaker.h"

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation SessionDetails
@synthesize selectedSession;
@synthesize lblSessionTime;
@synthesize lblSpeakerName;
@synthesize lblSessionTitle;
@synthesize lblRoom;
@synthesize imgSpeaker;
@synthesize txtSessionDesc;
@synthesize appDelegate;
@synthesize btnViewSpeakerInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Speaker* speaker = [selectedSession speaker];
    lblSessionTitle.text = [selectedSession session];
    txtSessionDesc.text = [selectedSession desc];
    lblSessionTime.text = [selectedSession time];
    lblRoom.text = [selectedSession room];
    lblSpeakerName.text = [speaker name];
    
    NSString* imgUrl = [NSString stringWithFormat:@"http://www.NebraskaCodeCamp.com/Content/Speakers/%@", [speaker img]];
    
    AsyncImageDownload* asyncImg = [[AsyncImageDownload alloc] init];
    [asyncImg loadImageFromURL:imgUrl :nil :self :nil];
    
    [asyncImg autorelease];
    
    self.navigationController.navigationBar.tintColor = UIColorFromRGB(0x9B1D20);
    btnViewSpeakerInfo.titleLabel.textColor = UIColorFromRGB(0x9B1D20);
    [btnViewSpeakerInfo setTitleColor:UIColorFromRGB(0x9B1D20) forState:UIControlStateNormal];
    [btnViewSpeakerInfo setTitleColor:UIColorFromRGB(0x9B1D20) forState:UIControlStateHighlighted];
}

- (void) setImageForGivenIndexPath:(UIImage *)img:(NSIndexPath *)indexInfo:(UIImageView *)imgview
{
    imgSpeaker.frame = CGRectMake(5, 43, 90, 117);
    [imgSpeaker setImage:img];
}

- (IBAction) showSpeakerDetails: (id) sender {
    [appDelegate launchSpeakerDetailsView:[selectedSession speaker]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
