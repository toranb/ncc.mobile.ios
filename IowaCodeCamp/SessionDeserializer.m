#import "SessionDeserializer.h"
#import "Session.h"
#import "Speaker.h"
#import "JSON.h"

@implementation SessionDeserializer

- (NSArray *)parseJsonAndReturnSessions:(NSString *)html {
    NSArray* json = [html JSONValue];
    
    NSArray* items = [json valueForKeyPath:@"d.data"];
    
    NSMutableArray* sessions = [NSMutableArray array];
    for (NSDictionary *item in items) {
        Session* session = [[Session alloc] init];
        
        [session setSession:[item objectForKey:@"Session"]];
        [session setTime:[item objectForKey:@"Time"]];
        [session setDesc:[item objectForKey:@"Desc"]];
        [session setRoom:[item objectForKey:@"Room"]];
        
        NSDictionary *spkr = [item objectForKey:@"Speaker"];
        NSString* name = [spkr objectForKey:@"Name"];
        NSString* bio = [spkr objectForKey:@"Bio"];
        NSString* img = [spkr objectForKey:@"Img"];
        NSString* web = [spkr objectForKey:@"Web"];
        NSString* location = [spkr objectForKey:@"Location"];
        
        Speaker* speaker = [[Speaker alloc] init];
        [speaker setName:name];
        [speaker setBio:bio];
        [speaker setImg:img];
        [speaker setWeb:web];
        [speaker setLocation:location];
        
        [session setSpeaker:speaker];
        
        [sessions addObject:session];
        
        [speaker release];
        [session release];
    }
    
    return sessions;
}

@end
