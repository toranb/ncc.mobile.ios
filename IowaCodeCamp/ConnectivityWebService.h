#import <Foundation/Foundation.h>

@class NebraskaCodeCampAppDelegate;

@interface ConnectivityWebService : NSObject {
    NSMutableData* responseData;
    NebraskaCodeCampAppDelegate* appDelegate;
}

@property (nonatomic, retain) NebraskaCodeCampAppDelegate* appDelegate;
@property (nonatomic, retain) NSMutableData* responseData;

- (id) initWithAppDelegate:(NebraskaCodeCampAppDelegate *)apDelegate;
- (void) verifyConnectivity;

@end
